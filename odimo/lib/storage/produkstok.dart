import 'dart:convert';

class Produkstok {
  String id;
  String kategori;
  String foto;
  String nama_product;
  String stok;
  String deskripsi;
  String createddate;
  var encoddecodeBase64Image;
  Produkstok(
      {this.id,
      this.kategori,
      this.foto,
      this.nama_product,
      this.stok,
      this.deskripsi,
      this.createddate,
      this.encoddecodeBase64Image});

  Produkstok.fromJson(Map<String, dynamic> json) {
    this.id = json['id'].toString();
    this.kategori = json['kategori'].toString();
    this.foto = json['foto'].toString();
    this.nama_product = json['nama_product'].toString();
    this.stok = json['stok'].toString();
    this.deskripsi =
        json['deskripsi'] == null ? '-' : json['deskripsi'].toString();
    this.createddate = json['createddate'].toString();
    this.encoddecodeBase64Image = base64.decode(json['foto'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['kategori'] = this.kategori;
    data['foto'] = this.foto;
    data['nama_product'] = this.nama_product;
    data['stok'] = this.stok;
    data['deskripsi'] = this.deskripsi;
    data['createddate'] = this.createddate;

    return data;
  }
}
