
class Transaction{
  String id;
  String type_transactions;
  String type;
  String nama_product;
  String harga;
  String qty;
  String createddate;
  Transaction({this.id, this.type_transactions, this.type, this.nama_product, this.harga, this.qty, this.createddate});


  Transaction.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.type_transactions = json['type_transactions'].toString();
    this.type = json['tipe'].toString();
    this.nama_product = json['nama_product'].toString();
    this.harga = json['harga'].toString();
    this.qty = json['qty'].toString();
    this.createddate = json['createddate'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type_transactions'] = this.type_transactions;
    data['tipe'] = this.type;
    data['nama_product'] = this.nama_product;
    data['harga'] = this.harga;
    data['qty'] = this.qty;
    data['createddate'] = this.createddate;

    return data;
  }
}