import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/modules/pembelian/controllers/bloc/listpembelian.bloc.dart';
import 'package:template/modules/pembelian/controllers/event/listpembelian.event.dart';
import 'package:template/modules/pembelian/controllers/state/listpembelian.state.dart';
import 'package:template/modules/pembelian/models/listpembelian.model.dart';

class ListPembelian extends StatefulWidget {
  ListPembelian({Key key}) : super(key: key);

  @override
  _ListPembelianState createState() => _ListPembelianState();
}

class _ListPembelianState extends State<ListPembelian> {
  ListPembelianBloc _bloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc = ListPembelianBloc();

    ListPembelianModel.isLoading = true;
    ListPembelianModel.page = "1";
    ListPembelianModel.data.clear();
    _bloc.add(GetDataProduk());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Daftar Pembelian"),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: NotificationListener<ScrollNotification>(
                    onNotification: (ScrollNotification scrollInfo) {
                      if (scrollInfo.metrics.pixels ==
                          scrollInfo.metrics.maxScrollExtent) {
                        if (!ListPembelianModel.isLoading) {
                          if (ListPembelianModel.page != 'last') {
                            ListPembelianModel.isLoading = true;
                            _bloc.add(GetDataProduk());
                          }
                        }
                      }

                      return true;
                    },
                    child: BlocBuilder(
                      bloc: _bloc,
                      builder: (context, state) {
                        if (state is FetchDataProdukSuccess) {
                          return ListView.builder(
                              itemCount: state.data.length,
                              shrinkWrap: true,
                              itemBuilder: (context, item) {
                                String type = "";
                                if (state.data[item].type_transactions == "3") {
                                  type = "TRANSFER";
                                }
                                if (state.data[item].type_transactions == "2") {
                                  type = "KREDIT";
                                }
                                if (state.data[item].type_transactions == "1") {
                                  type = "CASH";
                                }

                                return Container(
                                    padding: EdgeInsets.only(left: 8, right: 8),
                                    height: 120,
                                    width: double.maxFinite,
                                    child: Flexible(
                                      flex: 1,
                                      child: Card(
                                        child: Padding(
                                          padding: EdgeInsets.all(6),
                                          child: Stack(
                                            children: <Widget>[
                                              Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Stack(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 5),
                                                      child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            3),
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerLeft,
                                                                  child: Icon(
                                                                    Icons
                                                                        .add_shopping_cart,
                                                                    color: Colors
                                                                        .brown,
                                                                    size: 25,
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                height: 10,
                                                              ),
                                                              Align(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: RichText(
                                                                  text:
                                                                      TextSpan(
                                                                    text: state
                                                                        .data[
                                                                            item]
                                                                        .nama_product,
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Colors
                                                                            .black,
                                                                        fontSize:
                                                                            14),
                                                                    children: <
                                                                        TextSpan>[
                                                                      TextSpan(
                                                                          text:
                                                                              "\nOdimo.id",
                                                                          style: TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.bold)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              Spacer(),
                                                              Align(
                                                                alignment:
                                                                    Alignment
                                                                        .topRight,
                                                                child: RichText(
                                                                  text:
                                                                      TextSpan(
                                                                    text: "Qty",
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Colors
                                                                            .green,
                                                                        fontSize:
                                                                            16),
                                                                    children: <
                                                                        TextSpan>[
                                                                      TextSpan(
                                                                          text:
                                                                              "\n${state.data[item].qty.toString()} Pcs",
                                                                          style: TextStyle(
                                                                              color: Colors.green,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.bold)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          Row(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      left: 8),
                                                                  child: Row(
                                                                    children: <
                                                                        Widget>[
                                                                      RichText(
                                                                          textAlign: TextAlign
                                                                              .left,
                                                                          text: TextSpan(
                                                                              text: "\nRp, ${state.data[item].harga.toString()}",
                                                                              style: TextStyle(color: Colors.grey, fontSize: 14),
                                                                              children: <TextSpan>[
                                                                                TextSpan(text: "\n${state.data[item].createddate.toString()}", style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic, fontSize: 12, fontWeight: FontWeight.bold)),
                                                                                TextSpan(text: "\n${type}", style: TextStyle(color: type == 'CASH' ? Colors.green : type == 'KREDIT' ? Colors.red : Colors.blue, fontStyle: FontStyle.italic, fontSize: 12, fontWeight: FontWeight.bold)),
                                                                              ]))
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ));
                              });
                        }

                        return ListView.builder(
                            itemCount: 0,
                            itemBuilder: (context, item) {
                              return Text("");
                            });
                      },
                    )),
              ),
              BlocBuilder(
                  bloc: _bloc,
                  builder: (context, state) {
                    if (state is FethDataProduk) {
                      print("state " + state.isLoading.toString());
                      return Container(
                        height: state.isLoading ? 50.0 : 0,
                        color: Colors.transparent,
                        child: Center(
                          child: new CircularProgressIndicator(),
                        ),
                      );
                    } else {
                      return Container();
                    }
                  })
            ],
          ),
        ),
        floatingActionButton: BottomSheet(
            onClosing: () {},
            builder: (context) {
              return Container(
                height: 63,
                margin: EdgeInsets.only(left: 32),
                width: double.maxFinite,
                child: Card(
                    elevation: 5,
                    color: Colors.brown,
                    child: BlocBuilder(
                      bloc: _bloc,
                      builder: (context, state) {
                        if (state is FetchDataProdukSuccess) {
                          return Padding(
                            padding: const EdgeInsets.all(16),
                            child: Text(
                              "TOTAL PENJUALAN : Rp, ${state.total_penjualan}\nJUMLAH : ${state.total_terjual} PCS",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            ),
                          );
                        }

                        return Container();
                      },
                    )),
              );
            }));
  }
}
