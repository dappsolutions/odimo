import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/modules/pembelian/controllers/bloc/pembelian.bloc.dart';
import 'package:template/modules/pembelian/controllers/event/pembelian.event.dart';
import 'package:template/modules/pembelian/controllers/state/pembelian.state.dart';
import 'package:template/modules/pembelian/models/pembelian.models.dart';
import 'package:template/routes/navigator.dart';

class Pembelian extends StatefulWidget {
  Pembelian({Key key}) : super(key: key);

  @override
  _PembelianState createState() => _PembelianState();
}

class _PembelianState extends State<Pembelian> {
  PembelianBloc _bloc;

  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  @override
  void initState() {
    super.initState();
    _bloc = new PembelianBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pembelian Odimo"),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (choice) {
              if (choice == "Daftar Pembelian") {
                Navigate.route[PembelianModel.modules]
                    ["daftar_pembelian"](context);
              }

              if (choice == "Daftar Produk Stok") {
                Navigate.route[PembelianModel.modules]
                    ["daftar_produk_stok"](context);
              }

              if (choice == "Input Stok") {
                Navigate.route[PembelianModel.modules]["inputstok"](context);
              }

              if (choice == "Input Pengeluaran") {
                Navigate.route[PembelianModel.modules]
                    ["inputpengeluaran"](context);
              }
            },
            itemBuilder: (BuildContext context) {
              return PembelianModel.morelist.map((String choice) {
                return PopupMenuItem(value: choice, child: Text(choice));
              }).toList();
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 30.0)),
              Text(
                'Transaksi Pembelian Odimo',
                style: TextStyle(color: hexToColor("#806949"), fontSize: 25.0),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.all(16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.bookmark),
                      labelText: "Masukkan Barang",
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide())),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Barang Harus Diisi";
                    }

                    return null;
                  },
                  keyboardType: TextInputType.text,
                  style: TextStyle(fontFamily: "Poppins"),
                  onChanged: (val) {
                    PembelianModel.params["nama_barang"] = val.toString();
                  },
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(right: 16, left: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: "Masukkan Harga Barang",
                      prefixIcon: Icon(Icons.monetization_on),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide())),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Harga Barang Harus Diisi";
                    }

                    return null;
                  },
                  keyboardType: TextInputType.number,
                  style: TextStyle(fontFamily: "Poppins"),
                  onChanged: (val) {
                    PembelianModel.params["harga"] = val.toString();
                  },
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(right: 16, left: 16, top: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      labelText: "Masukkan Jumlah Barang",
                      prefixIcon: Icon(Icons.shopping_cart),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide())),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Jumlah Barang Harus Diisi";
                    }

                    return null;
                  },
                  keyboardType: TextInputType.number,
                  style: TextStyle(fontFamily: "Poppins"),
                  onChanged: (val) {
                    PembelianModel.params["qty"] = val.toString();
                  },
                ),
              ),
              Container(
                  height: 55,
                  // width: 300,
                  margin: EdgeInsets.only(right: 16, left: 16, top: 16),
                  child: InputDecorator(
                    decoration:
                        const InputDecoration(border: OutlineInputBorder()),
                    child: DropdownButtonHideUnderline(
                        child: BlocBuilder(
                            bloc: _bloc,
                            builder: (context, state) {
                              if (state is SetChangeValueTypeTransaction) {
                                return DropdownButton(
                                  items: PembelianModel.type_transactions
                                      .map((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                  value: state.value,
                                  onChanged: (val) {
                                    PembelianModel.default_trans = val;
                                    _bloc.add(ChangeValueTypeTransaction());
                                  },
                                );
                              }

                              return DropdownButton(
                                items: PembelianModel.type_transactions
                                    .map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                value: PembelianModel.default_trans,
                                onChanged: (val) {
                                  PembelianModel.default_trans = val;
                                  _bloc.add(ChangeValueTypeTransaction());
                                },
                              );
                            })),
                  )),
              BlocBuilder(
                bloc: _bloc,
                builder: (context, state) {
                  if (state is BeforeSimpanPembelian) {
                    return Container(
                      height: 50.0,
                      color: Colors.transparent,
                      child: Center(
                        child: new CircularProgressIndicator(),
                      ),
                    );
                  }

                  return Container();
                },
              ),
              BlocBuilder(
                bloc: _bloc,
                builder: (context, state) {
                  if (state is SimpanPembelianSuccess) {
                    return Container(
                      child: AlertDialog(
                        title: Text("Informasi"),
                        content: Text(state.message.toString()),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('OK'),
                            onPressed: () {},
                          ),
                        ],
                      ),
                    );
                  }

                  return Container();
                },
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          PembelianModel.params['type_trans'] = PembelianModel.default_trans;
          _bloc.add(SimpanPembelian());
        },
        icon: Icon(Icons.save),
        label: Text("Simpan"),
      ),
    );
  }
}
