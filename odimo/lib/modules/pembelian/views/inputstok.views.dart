import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/modules/pembelian/controllers/bloc/inputstok.bloc.dart';
import 'package:template/modules/pembelian/controllers/event/inputstok.event.dart';
import 'package:template/modules/pembelian/controllers/state/inputstok.state.dart';
import 'package:template/modules/pembelian/models/inputstok.models.dart';

class InputStok extends StatefulWidget {
  InputStok({Key key}) : super(key: key);

  @override
  _InputStokState createState() => _InputStokState();
}

class _InputStokState extends State<InputStok> {
  // File _image;
  // final picker = ImagePicker();
  // // var gmbr_decode = null;

  // Future getImage() async {
  //   final pickedFile = await picker.getImage(source: ImageSource.gallery);
  //   // final bytes = await File(pickedFile.path).readAsBytes();

  //   // String gmbr = base64.encode(bytes);

  //   setState(() {
  //     _image = File(pickedFile.path);
  //     // gmbr_decode = base64.decode(gmbr);
  //   });
  // }
  InputStokBloc _bloc;

  @override
  void initState() {
    _bloc = new InputStokBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Input Stok Produk"),
      ),
      body: SingleChildScrollView(
        child: BlocBuilder(
          bloc: _bloc,
          builder: (context, state) {
            if (state is SimpanStokSuccess) {
              return Container(
                child: AlertDialog(
                  title: Text("Informasi"),
                  content: Text(state.message.toString()),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('OK'),
                      onPressed: () {},
                    ),
                  ],
                ),
              );
            }

            if (state is BeforeSimpanStok) {
              return Container(
                height: 50.0,
                color: Colors.transparent,
                child: Center(
                  child: new CircularProgressIndicator(),
                ),
              );
            }

            if (state is SimpanStokFailed) {
              return Container(
                height: 50.0,
                color: Colors.transparent,
                child: Center(child: Text(state.error)),
              );
            }

            return Container(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 3),
                    child: RaisedButton(
                        child: Text(
                          "Input File From Gallery",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.orange,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        onPressed: () {
                          _bloc.add(TakePictFromGallery());
                        }),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 6),
                    child: Center(
                        child: BlocBuilder(
                            bloc: _bloc,
                            builder: (context, state) {
                              if (state is OutputImageFromGallery) {
                                return Image.file(state.image);
                              }
                              return Image.asset('assets/no_image.jpg');
                            })),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: 50,
                    margin: EdgeInsets.only(left: 16, right: 16, top: 6),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.bookmark),
                          labelText: "Masukkan Nama Produk",
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide())),
                      validator: (val) {
                        if (val.length == 0) {
                          return "Produk Harus Diisi";
                        }

                        return null;
                      },
                      keyboardType: TextInputType.text,
                      style: TextStyle(fontFamily: "Poppins"),
                      onChanged: (val) {
                        InputStokModel.nama_product = val.toString();
                      },
                    ),
                  ),
                  Container(
                      height: 55,
                      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
                      child: InputDecorator(
                        decoration:
                            InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: BlocBuilder(
                                bloc: _bloc,
                                builder: (context, state) {
                                  if (state is ChangeKategoriProduk) {
                                    return DropdownButton(
                                      items: InputStokModel.kategori_produk
                                          .map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      value: state.kategori,
                                      onChanged: (val) {
                                        InputStokModel.default_kategori = val;
                                        _bloc.add(
                                            ChangeValueDefaultKategoriProduk());
                                      },
                                    );
                                  }

                                  return DropdownButton(
                                    items: InputStokModel.kategori_produk
                                        .map((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    value: InputStokModel.default_kategori,
                                    onChanged: (val) {
                                      InputStokModel.default_kategori = val;
                                      _bloc.add(
                                          ChangeValueDefaultKategoriProduk());
                                    },
                                  );
                                })),
                      )),
                  Container(
                      height: 55,
                      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
                      child: InputDecorator(
                        decoration:
                            InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                            child: BlocBuilder(
                                bloc: _bloc,
                                builder: (context, state) {
                                  if (state is ChangeTipe) {
                                    return DropdownButton(
                                      items: InputStokModel.tipe
                                          .map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      value: state.tipe,
                                      onChanged: (val) {
                                        InputStokModel.default_tipe = val;
                                        _bloc.add(ChangeValueDefaultTipe());
                                      },
                                    );
                                  }

                                  return DropdownButton(
                                    items:
                                        InputStokModel.tipe.map((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    value: InputStokModel.default_tipe,
                                    onChanged: (val) {
                                      InputStokModel.default_tipe = val;
                                      _bloc.add(ChangeValueDefaultTipe());
                                    },
                                  );
                                })),
                      )),
                  Container(
                    height: 50,
                    margin: EdgeInsets.only(left: 16, right: 16, top: 6),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.bookmark),
                          labelText: "Masukkan Deskripsi",
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide())),
                      validator: (val) {
                        if (val.length == 0) {
                          return "Deskripsi Harus Diisi";
                        }

                        return null;
                      },
                      keyboardType: TextInputType.text,
                      style: TextStyle(fontFamily: "Poppins"),
                      onChanged: (val) {
                        InputStokModel.deskripsi = val.toString();
                      },
                    ),
                  ),
                  Container(
                    height: 50,
                    margin: EdgeInsets.only(left: 16, right: 16, top: 6),
                    child: TextFormField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.bookmark),
                          labelText: "Masukkan Stok",
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide())),
                      validator: (val) {
                        if (val.length == 0) {
                          return "Stok Harus Diisi";
                        }

                        return null;
                      },
                      keyboardType: TextInputType.number,
                      style: TextStyle(fontFamily: "Poppins"),
                      onChanged: (val) {
                        InputStokModel.stok = val.toString();
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          InputStokModel.params['nama_product'] = InputStokModel.nama_product;
          InputStokModel.params['encodeimages'] = InputStokModel.encodeImages;
          InputStokModel.params['stok'] = InputStokModel.stok;
          InputStokModel.params['kategori'] = InputStokModel.default_kategori;
          InputStokModel.params['deskripsi'] = InputStokModel.deskripsi;
          InputStokModel.params['tipe'] = InputStokModel.default_tipe;
          _bloc.add(SimpanInputStok());
        },
        icon: Icon(Icons.save),
        label: Text("Simpan"),
      ),
    );
  }
}
