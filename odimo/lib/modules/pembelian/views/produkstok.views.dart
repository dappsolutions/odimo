import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:template/modules/pembelian/controllers/bloc/produkstok.bloc.dart';
import 'package:template/modules/pembelian/controllers/event/produkstok.event.dart';
import 'package:template/modules/pembelian/controllers/state/produkstok.state.dart';
import 'package:template/modules/pembelian/models/inputstok.models.dart';
import 'package:template/modules/pembelian/models/produkstok.models.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:social_share/social_share.dart';

class ProdukStok extends StatefulWidget {
  ProdukStok({Key key}) : super(key: key);

  @override
  _ProdukStokState createState() => _ProdukStokState();
}

class _ProdukStokState extends State<ProdukStok> {
  ProdukStokBloc _bloc;

  Future<void> requestPermission(Permission permission) async {
    final status = await permission.request();
  }

  @override
  void initState() {
    super.initState();
    requestPermission(Permission.storage);

    _bloc = ProdukStokBloc();
    ProdukStokModel.isLoading = true;
    ProdukStokModel.page = "1";
    ProdukStokModel.data.clear();
    _bloc.add(GetDataProduk());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Daftar Produk Stok"),
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected: (choice) {
                ProdukStokModel.isLoading = true;
                ProdukStokModel.page = "1";
                ProdukStokModel.data.clear();
                ProdukStokModel.filter_kategori = choice;
                if (choice == "SEMUA") {
                  ProdukStokModel.filter_kategori = "";
                }
                _bloc.add(GetDataProduk());
              },
              itemBuilder: (BuildContext context) {
                return ProdukStokModel.morelist.map((String choice) {
                  return PopupMenuItem(value: choice, child: Text(choice));
                }).toList();
              },
            )
          ],
        ),
        body: BlocBuilder(
          bloc: _bloc,
          builder: (context, state) {
            if (state is FetchDataProdukError || state is SimpanStokFailed) {
              return Center(
                child: Text(state.error),
              );
            }

            return Container(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: NotificationListener<ScrollNotification>(
                        onNotification: (ScrollNotification scrollInfo) {
                          if (scrollInfo.metrics.pixels ==
                              scrollInfo.metrics.maxScrollExtent) {
                            if (!ProdukStokModel.isLoading) {
                              if (ProdukStokModel.page != 'last') {
                                ProdukStokModel.isLoading = true;
                                _bloc.add(GetDataProduk());
                              }
                            }
                          }

                          return true;
                        },
                        child: BlocBuilder(
                          bloc: _bloc,
                          builder: (context, state) {
                            if (state is FetchDataProdukSuccess) {
                              return ListView.builder(
                                  itemCount: state.data.length,
                                  itemBuilder: (context, item) {
                                    return Container(
                                      padding:
                                          EdgeInsets.only(left: 8, right: 8),
                                      height: 400,
                                      width: double.maxFinite,
                                      child: Card(
                                        child: Padding(
                                          padding: EdgeInsets.all(6),
                                          child: Stack(
                                            children: <Widget>[
                                              Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Stack(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 5),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 250,
                                                            child: Align(
                                                                alignment:
                                                                    Alignment
                                                                        .topCenter,
                                                                child: Image
                                                                    .memory(state
                                                                        .data[
                                                                            item]
                                                                        .encoddecodeBase64Image)),
                                                          ),
                                                          SizedBox(
                                                            height: 20,
                                                          ),
                                                          Divider(),
                                                          Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            3),
                                                                child: Align(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerLeft,
                                                                  child: Icon(
                                                                    Icons
                                                                        .bookmark,
                                                                    color: Colors
                                                                        .brown,
                                                                    size: 35,
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                height: 10,
                                                              ),
                                                              Align(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: RichText(
                                                                  text:
                                                                      TextSpan(
                                                                    text:
                                                                        "${state.data[item].nama_product.toUpperCase()}",
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Colors
                                                                            .black,
                                                                        fontSize:
                                                                            16),
                                                                    children: <
                                                                        TextSpan>[
                                                                      TextSpan(
                                                                          text:
                                                                              "\n${state.data[item].kategori}",
                                                                          style: TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.bold)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              Spacer(),
                                                              Align(
                                                                alignment:
                                                                    Alignment
                                                                        .topRight,
                                                                child: RichText(
                                                                  text:
                                                                      TextSpan(
                                                                    text:
                                                                        "Stok",
                                                                    style: TextStyle(
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        color: Colors
                                                                            .green,
                                                                        fontSize:
                                                                            16),
                                                                    children: <
                                                                        TextSpan>[
                                                                      TextSpan(
                                                                          text:
                                                                              "\n${state.data[item].stok} Pcs",
                                                                          style: TextStyle(
                                                                              color: Colors.green,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.bold)),
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          Row(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      left: 8),
                                                                  child: Row(
                                                                    children: <
                                                                        Widget>[
                                                                      RichText(
                                                                          textAlign: TextAlign
                                                                              .left,
                                                                          text:
                                                                              TextSpan(children: <TextSpan>[
                                                                            TextSpan(
                                                                                text: "\n${state.data[item].deskripsi}\nODIMO.ID",
                                                                                style: TextStyle(color: Colors.red, fontStyle: FontStyle.italic, fontSize: 12, fontWeight: FontWeight.bold)),
                                                                          ])),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                              Spacer(),
                                                              GestureDetector(
                                                                child: Icon(
                                                                    Icons.list),
                                                                onTap: () {
                                                                  showMenu(
                                                                      context:
                                                                          context,
                                                                      position:
                                                                          RelativeRect.fromLTRB(
                                                                              400,
                                                                              400,
                                                                              0,
                                                                              0),
                                                                      items: <
                                                                          PopupMenuEntry>[
                                                                        PopupMenuItem(
                                                                          value:
                                                                              0,
                                                                          child:
                                                                              Row(
                                                                            children: <Widget>[
                                                                              Icon(Icons.check),
                                                                              GestureDetector(
                                                                                child: Text("Change Stok"),
                                                                                onTap: () {
                                                                                  Navigator.pop(context);
                                                                                  showDialogChangeStok(state.data[item].id);
                                                                                },
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        PopupMenuItem(
                                                                          value:
                                                                              0,
                                                                          child:
                                                                              Row(
                                                                            children: <Widget>[
                                                                              Icon(Icons.file_download),
                                                                              GestureDetector(
                                                                                child: Text("Simpan Gambar"),
                                                                                onTap: () {
                                                                                  Navigator.pop(context);
                                                                                  _bloc.add(SimpanGambarStok(encodeImage: state.data[item].encoddecodeBase64Image, codeDaster: state.data[item].id.toString()));
                                                                                  Scaffold.of(context).showSnackBar(SnackBar(content: Text('Download Sukses')));
                                                                                  // showDialogChangeStok(state.data[item].id);
                                                                                },
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        PopupMenuItem(
                                                                          value:
                                                                              0,
                                                                          child:
                                                                              Row(
                                                                            children: <Widget>[
                                                                              Icon(Icons.share),
                                                                              GestureDetector(
                                                                                child: Text("Bagikan"),
                                                                                onTap: () async {
                                                                                  Navigator.pop(context);

                                                                                  String dir = (await getApplicationDocumentsDirectory()).path;
                                                                                  // print(dir);
                                                                                  String fullPath = '$dir/DASTER00-${state.data[item].id}.png';
                                                                                  File file = File(fullPath);
                                                                                  await file.writeAsBytes(state.data[item].encoddecodeBase64Image);

                                                                                  SocialShare.shareWhatsapp("Pembelian Odimo.id").then((data) {
                                                                                    print(data);
                                                                                  });
                                                                                },
                                                                              )
                                                                            ],
                                                                          ),
                                                                        )
                                                                      ]);
                                                                },
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  });
                            }

                            return ListView.builder(
                                itemCount: 0,
                                itemBuilder: (context, item) {
                                  return Text("");
                                });
                          },
                        )),
                  ),
                  BlocBuilder(
                      bloc: _bloc,
                      builder: (context, state) {
                        if (state is FethDataProduk) {
                          return Container(
                            height: state.isLoading ? 50.0 : 0,
                            color: Colors.transparent,
                            child: Center(
                              child: new CircularProgressIndicator(),
                            ),
                          );
                        }
                        return Container();
                      }),
                  BlocBuilder(
                      bloc: _bloc,
                      builder: (context, state) {
                        if (state is BeforeSimpanStokProduk) {
                          return Container(
                            height: 50.0,
                            color: Colors.transparent,
                            child: Center(
                              child: new CircularProgressIndicator(),
                            ),
                          );
                        }
                        return Container();
                      })
                ],
              ),
            );
          },
        ),
        floatingActionButton: BottomSheet(
            onClosing: () {},
            builder: (context) {
              return Container(
                height: 63,
                margin: EdgeInsets.only(left: 32),
                width: double.maxFinite,
                child: Card(
                    elevation: 5,
                    color: Colors.brown,
                    child: BlocBuilder(
                      bloc: _bloc,
                      builder: (context, state) {
                        if (state is FetchDataProdukSuccess) {
                          return Padding(
                            padding: const EdgeInsets.all(16),
                            child: Text(
                              "TOTAL STOK : ${state.total_stok} PCS",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            ),
                          );
                        }

                        return Container();
                      },
                    )),
              );
            }));
  }

  Future showDialogChangeStok(String product_stok) async {
    await showDialog<String>(
        context: context,
        child: AlertDialog(
          contentPadding: EdgeInsets.all(16),
          content: Row(
            children: <Widget>[
              new Expanded(
                child: new TextField(
                  autofocus: true,
                  decoration: new InputDecoration(
                      labelText: 'UBAH STOK', hintText: 'STOK'),
                  keyboardType: TextInputType.number,
                  onChanged: (val) {
                    ProdukStokModel.stok = val;
                  },
                ),
              )
            ],
          ),
          actions: <Widget>[
            new FlatButton(
                child: const Text('BATAL'),
                onPressed: () {
                  Navigator.pop(context);
                }),
            new FlatButton(
                child: const Text('SIMPAN'),
                onPressed: () {
                  // print(product_stok + " - " + ProdukStokModel.stok);
                  Navigator.pop(context);
                  ProdukStokModel.params['product_stok_id'] = product_stok;
                  ProdukStokModel.params['stok'] = ProdukStokModel.stok;
                  _bloc.add(SimpanStokProduk());
                })
          ],
        ));
  }
}
