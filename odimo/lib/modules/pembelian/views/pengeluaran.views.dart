import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/modules/pembelian/controllers/bloc/pengeluaran.bloc.dart';
import 'package:template/modules/pembelian/controllers/event/pengeluaran.event.dart';
import 'package:template/modules/pembelian/controllers/state/pengeluaran.state.dart';
import 'package:template/modules/pembelian/models/pengeluaran.models.dart';

class Pengeluaran extends StatefulWidget {
  Pengeluaran({Key key}) : super(key: key);

  @override
  _PengeluaranState createState() => _PengeluaranState();
}

class _PengeluaranState extends State<Pengeluaran> {
  Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  PengeluaranBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = PengeluaranBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Input pengeluaran"),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 30.0)),
              Container(
                height: 50,
                margin: EdgeInsets.all(16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.bookmark),
                      labelText: "Masukkan Keterangan Pengeluaran",
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide())),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Pengeluaran Harus Diisi";
                    }

                    return null;
                  },
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(fontFamily: "Poppins"),
                  onChanged: (val) {
                    PengeluaranModel.ket = val;
                  },
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(left: 16, right: 16),
                child: TextFormField(
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.bookmark),
                      labelText: "Masukkan Jumlah ",
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          borderSide: BorderSide())),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Jumlah Harus Diisi";
                    }

                    return null;
                  },
                  keyboardType: TextInputType.number,
                  style: TextStyle(fontFamily: "Poppins"),
                  onChanged: (val) {
                    PengeluaranModel.jumlah = val.toString();
                  },
                ),
              ),
              BlocBuilder(
                bloc: _bloc,
                builder: (context, state) {
                  if (state is BeforeSimpanPengeluaran) {
                    return Container(
                      height: 50.0,
                      color: Colors.transparent,
                      child: Center(
                        child: new CircularProgressIndicator(),
                      ),
                    );
                  }

                  return Container();
                },
              ),
              BlocBuilder(
                bloc: _bloc,
                builder: (context, state) {
                  if (state is SimpanPengeluaranSuccess) {
                    return Container(
                      child: AlertDialog(
                        title: Text("Informasi"),
                        content: Text(state.message.toString()),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('OK'),
                            onPressed: () {},
                          ),
                        ],
                      ),
                    );
                  }

                  return Container();
                },
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          PengeluaranModel.params['keterangan'] = PengeluaranModel.ket;
          PengeluaranModel.params['jumlah'] = PengeluaranModel.jumlah;
          _bloc.add(SimpanPengeluaran());
        },
        icon: Icon(Icons.save),
        label: Text("Simpan"),
      ),
    );
  }
}
