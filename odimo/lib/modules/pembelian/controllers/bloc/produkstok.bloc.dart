import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:template/modules/pembelian/controllers/event/produkstok.event.dart';
import 'package:template/modules/pembelian/controllers/state/produkstok.state.dart';
import 'package:template/modules/pembelian/models/inputstok.models.dart';
import 'dart:convert';

import 'package:template/modules/pembelian/models/produkstok.models.dart';
import 'package:template/routes/api.dart';
import 'package:template/storage/produkstok.dart';

import 'package:image_gallery_saver/image_gallery_saver.dart';

class ProdukStokBloc extends Bloc<ProdukStokEvent, ProdukStokState> {
  @override
  ProdukStokState get initialState => FethDataProduk(isLoading: false);

  @override
  Stream<ProdukStokState> mapEventToState(ProdukStokEvent event) async* {
    if (event is GetDataProduk) {
      print(event);
      bool isLoading = ProdukStokModel.isLoading;
      yield FethDataProduk(isLoading: isLoading);
      try {
        if (isLoading) {
          String page = ProdukStokModel.page;
          String filter_kategori = ProdukStokModel.filter_kategori;
          if (page != 'last') {
            var request = await http.get(Api.route[InputStokModel.modules]
                ['getDataForAdmin'](page, filter_kategori));
            // ['getData'](page, filter_kategori));

            if (request.statusCode == 200) {
              var data = json.decode(request.body);
              List<Produkstok> dataAdd = [];
              for (var item in data['data']) {
                dataAdd.add(Produkstok.fromJson(item));
              }

              // print(dataAdd);
              ProdukStokModel.data.addAll(dataAdd);
              yield FetchDataProdukSuccess(
                data: ProdukStokModel.data,
                total_stok: data['total_stok'].toString(),
              );

              ProdukStokModel.page = data['next_page'].toString();
              ProdukStokModel.isLoading = false;
            } else {
              yield FetchDataProdukError(error: request.statusCode.toString());
            }
          } else {
            ProdukStokModel.isLoading = false;
          }
        }
      } catch (err) {
        // print("errR " + err.toString());
        yield FetchDataProdukError(error: err.toString());
      }
    }

    //simpan stok
    if (event is SimpanStokProduk) {
      yield BeforeSimpanStokProduk();
      try {
        var request = await http.post(
            Api.route[InputStokModel.modules]['simpan_stok'],
            body: ProdukStokModel.params);
        if (request.statusCode == 200) {
          var data = json.decode(request.body);
          // yield SimpanStokSuccess(message: data['message']);
          ProdukStokModel.isLoading = true;
          ProdukStokModel.page = "1";
          ProdukStokModel.data.clear();
          this.add(GetDataProduk());
        } else {
          yield SimpanStokFailed(error: request.statusCode.toString());
        }
      } catch (e) {
        yield SimpanStokFailed(error: e.toString());
      }
    }

    //Simpan Gambar Stok
    if (event is SimpanGambarStok) {
      //hasil decode from base64
      var encodeImage = event.encodeImage;
      String codeDaster = event.codeDaster;

      String dir = (await getApplicationDocumentsDirectory()).path;
      print(dir);
      String fullPath = '$dir/DASTER00-${codeDaster}.png';
      File file = File(fullPath);
      await file.writeAsBytes(encodeImage);

      // print(fullPath);
      final result = await ImageGallerySaver.saveFile(file.path);
      print(result);
    }
  }
}
