
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/modules/pembelian/controllers/event/pembelian.event.dart';
import 'package:template/modules/pembelian/controllers/state/pembelian.state.dart';
import 'package:template/modules/pembelian/models/pembelian.models.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:template/routes/api.dart';

class PembelianBloc extends Bloc<PembelianEvent, PembelianState>{

  @override
  PembelianState get initialState => InitTypeTransaction();

  @override
  Stream<PembelianState> mapEventToState(PembelianEvent event) async*{
    if(event is ChangeValueTypeTransaction){
      String type_trans = PembelianModel.default_trans;
      yield SetChangeValueTypeTransaction(value: type_trans);
    }

    if(event is SimpanPembelian){
      yield BeforeSimpanPembelian();
      try{        
        var request = await http.post(Api.route[PembelianModel.modules]['simpan'], body: PembelianModel.params);
        if(request.statusCode == 200){
          var data = json.decode(request.body);          
          yield SimpanPembelianSuccess(message: data['message']);
        }else{
          yield SimpanPembelianFailed(error: request.statusCode.toString());  
        }
      }catch(e){
        yield SimpanPembelianFailed(error: e.message.toString());
      }      
    }
  }
}