import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:template/modules/pembelian/controllers/event/listpembelian.event.dart';
import 'package:template/modules/pembelian/controllers/state/listpembelian.state.dart';
import 'package:template/modules/pembelian/models/listpembelian.model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:template/modules/pembelian/models/pembelian.models.dart';
import 'package:template/routes/api.dart';
import 'package:template/storage/transactions.dart';

class ListPembelianBloc extends Bloc<ListPembelianEvent, ListPembelianState> {
  @override
  ListPembelianState get initialState => FethDataProduk(isLoading: false);

  @override
  Stream<ListPembelianState> mapEventToState(ListPembelianEvent event) async* {
    if (event is GetDataProduk) {
      bool isLoading = ListPembelianModel.isLoading;
      yield FethDataProduk(isLoading: isLoading);
      try {
        if (isLoading) {
          String page = ListPembelianModel.page;                    
          if (page != 'last') {
            print("PAGE "+page);
            var request = await http
                .get(Api.route[PembelianModel.modules]['getData'](page));

            if (request.statusCode == 200) {
              var data = json.decode(request.body);
              List<Transaction> dataAdd = [];
              for (var item in data['data']) {
                dataAdd.add(Transaction.fromJson(item));
              }

              ListPembelianModel.data.addAll(dataAdd);
              yield FetchDataProdukSuccess(
                  data: ListPembelianModel.data,
                  total_terjual: data['barang_terjual'].toString(),
                  total_penjualan: data['total_pembelian'].toString());

              ListPembelianModel.page = data['next_page'].toString();
              ListPembelianModel.isLoading = false;
            } else {
              yield FetchDataProdukError(error: request.statusCode.toString());
            }
          }else{
            ListPembelianModel.isLoading = false;
          }
        }
      } catch (err) {
        print("errR " + err.toString());
        yield FetchDataProdukError(error: err.toString());
      }
    }
  }
}
