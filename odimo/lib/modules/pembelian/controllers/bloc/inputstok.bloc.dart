import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:template/modules/pembelian/controllers/event/inputstok.event.dart';
import 'package:template/modules/pembelian/controllers/state/inputstok.state.dart';
import 'package:template/modules/pembelian/models/inputstok.models.dart';
import 'dart:convert';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:template/routes/api.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

class InputStokBloc extends Bloc<InputStokEvent, InputStokState> {
  @override
  InputStokState get initialState => InitInputStok();

  @override
  Stream<InputStokState> mapEventToState(InputStokEvent event) async* {
    if (event is TakePictFromGallery) {
      final pickedFile =
          await ImagePicker().getImage(source: ImageSource.gallery);
      InputStokModel.image = File(pickedFile.path);

      final dir = await path_provider.getTemporaryDirectory();

      List<String> dataPath = pickedFile.path.split('/');

      String temporaryName =
          dataPath[dataPath.length - 1].replaceAll('.jpg', '');
      temporaryName = temporaryName.replaceAll('.png', '');
      temporaryName = temporaryName.replaceAll('.jpeg', '');
      temporaryName = temporaryName + "-temp.jpg";
      final targetPath = dir.absolute.path + "/${temporaryName}";
      // print("target path " + targetPath);
      // print("pickedFile.path " + dataPath[dataPath.length - 1]);
      InputStokModel.image = await FlutterImageCompress.compressAndGetFile(
        pickedFile.path,
        targetPath,
        quality: 50,
        minWidth: 450,
        minHeight: 450,
        // rotate: 90,
      );

      //encode base64
      final bytes = await InputStokModel.image.readAsBytes();
      InputStokModel.encodeImages = base64.encode(bytes);
      yield OutputImageFromGallery(image: InputStokModel.image);
    }

    if (event is SimpanInputStok) {
      yield BeforeSimpanStok();
      try {
        var request = await http.post(
            Api.route[InputStokModel.modules]['simpan'],
            body: InputStokModel.params);
        if (request.statusCode == 200) {
          var data = json.decode(request.body);
          yield SimpanStokSuccess(message: data['message']);
        } else {
          yield SimpanStokFailed(error: request.statusCode.toString());
        }
      } catch (e) {
        yield SimpanStokFailed(error: e.message.toString());
      }
    }

    if (event is ChangeValueDefaultKategoriProduk) {
      yield ChangeKategoriProduk(kategori: InputStokModel.default_kategori);
    }

    if (event is ChangeValueDefaultTipe) {
      yield ChangeTipe(tipe: InputStokModel.default_tipe);
    }
  }
}
