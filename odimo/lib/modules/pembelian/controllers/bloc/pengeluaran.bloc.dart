import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:template/modules/pembelian/controllers/event/pengeluaran.event.dart';
import 'package:template/modules/pembelian/controllers/state/pengeluaran.state.dart';
import 'package:template/modules/pembelian/models/pengeluaran.models.dart';
import 'dart:convert';

import 'package:template/routes/api.dart';

class PengeluaranBloc extends Bloc<PengeluaranEvent, PengeluaranState> {
  @override
  PengeluaranState get initialState => InitData();

  @override
  Stream<PengeluaranState> mapEventToState(PengeluaranEvent event) async* {
    if (event is SimpanPengeluaran) {
      yield BeforeSimpanPengeluaran();
      try {
        var request = await http.post(
            Api.route[PengeluaranModel.modules]['simpan'],
            body: PengeluaranModel.params);
        if (request.statusCode == 200) {
          var data = json.decode(request.body);
          yield SimpanPengeluaranSuccess(message: data['message']);
        } else {
          yield SimpanPengeluaranFailed(error: request.statusCode.toString());
        }
      } catch (e) {
        yield SimpanPengeluaranFailed(error: e.message.toString());
      }
    }
  }
}
