import 'package:equatable/equatable.dart';

abstract class PengeluaranState extends Equatable {
  const PengeluaranState();
}

class InitData extends PengeluaranState {
  @override
  List<Object> get props => [""];
}

class SetChangeValueTypeTransaction extends PengeluaranState {
  String value;

  SetChangeValueTypeTransaction({this.value});

  @override
  List<Object> get props => [value];
}

class BeforeSimpanPengeluaran extends PengeluaranState {
  @override
  List<Object> get props => [];
}

class SimpanPengeluaranFailed extends PengeluaranState {
  String error;
  SimpanPengeluaranFailed({this.error});

  @override
  List<Object> get props => [error];
}

class SimpanPengeluaranSuccess extends PengeluaranState {
  String message;
  SimpanPengeluaranSuccess({this.message});

  @override
  List<Object> get props => [message];
}
