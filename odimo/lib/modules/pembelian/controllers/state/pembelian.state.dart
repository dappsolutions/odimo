

import 'package:equatable/equatable.dart';

abstract class PembelianState extends Equatable{
  const PembelianState();  
}


class InitTypeTransaction extends PembelianState{
  @override
  List<Object> get props => ["CASH"];
}

class SetChangeValueTypeTransaction extends PembelianState{
  String value;  
  
  SetChangeValueTypeTransaction({this.value});

  @override
  List<Object> get props => [value];
}

class BeforeSimpanPembelian extends PembelianState{  
  @override
  List<Object> get props => [];
}

class SimpanPembelianFailed extends PembelianState{
  String error;
  SimpanPembelianFailed({this.error});

  @override
  List<Object> get props => [error];
}

class SimpanPembelianSuccess extends PembelianState{
  String message;
  SimpanPembelianSuccess({this.message});

  @override
  List<Object> get props => [message];
}


