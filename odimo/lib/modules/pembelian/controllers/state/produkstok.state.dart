import 'package:equatable/equatable.dart';
import 'package:template/storage/produkstok.dart';

abstract class ProdukStokState extends Equatable {
  const ProdukStokState();
}

class FethDataProduk extends ProdukStokState {
  bool isLoading;
  FethDataProduk({this.isLoading});

  @override
  List<Object> get props => [isLoading];
}

class FetchDataProdukSuccess extends ProdukStokState {
  List<Produkstok> data;
  String total_stok;

  FetchDataProdukSuccess({this.data, this.total_stok});

  @override
  List<Object> get props => [data, total_stok];
}

class FetchDataProdukError extends ProdukStokState {
  final String error;
  FetchDataProdukError({this.error});

  @override
  List<Object> get props => [error];
}

class GetJumlahTotalBarangStok extends ProdukStokState {
  String total;
  GetJumlahTotalBarangStok({this.total});

  @override
  List<Object> get props => [total];
}

class BeforeSimpanStokProduk extends ProdukStokState {
  bool isLoading;
  BeforeSimpanStokProduk({this.isLoading});

  @override
  List<Object> get props => [isLoading];
}

class SimpanStokFailed extends ProdukStokState {
  String error;
  SimpanStokFailed({this.error});

  @override
  List<Object> get props => [error];
}

class SimpanStokSuccess extends ProdukStokState {
  String message;
  SimpanStokSuccess({this.message});

  @override
  List<Object> get props => [message];
}
