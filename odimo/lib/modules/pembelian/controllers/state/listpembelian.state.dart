

import 'package:equatable/equatable.dart';
import 'package:template/storage/transactions.dart';

abstract class ListPembelianState extends Equatable{
  const ListPembelianState();
}

class FethDataProduk extends ListPembelianState{
  bool isLoading;
  FethDataProduk({this.isLoading});

  @override
  List<Object> get props => [isLoading];
}

class FetchDataProdukSuccess extends ListPembelianState{
  List<Transaction> data;
  String total_terjual, total_penjualan;

  FetchDataProdukSuccess({this.data, this.total_terjual, this.total_penjualan});

  @override
  List<Object> get props => [data, total_terjual, total_penjualan];
}

class FetchDataProdukError extends ListPembelianState{
  final String error;
  FetchDataProdukError({this.error});

  @override
  List<Object> get props => [error];
}

class GetJumlahTotalBarangTerjualAndTotalPembelian extends ListPembelianState{
  String total;
  String total_penjualan;
  GetJumlahTotalBarangTerjualAndTotalPembelian({this.total, this.total_penjualan});

  @override
  List<Object> get props => [total, total_penjualan];
}