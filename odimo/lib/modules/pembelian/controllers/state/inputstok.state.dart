import 'dart:io';

import 'package:equatable/equatable.dart';

abstract class InputStokState extends Equatable {
  const InputStokState();
}

class InitInputStok extends InputStokState {
  @override
  List<Object> get props => [""];
}

class OutputImageFromGallery extends InputStokState {
  File image;

  OutputImageFromGallery({this.image});

  @override
  List<Object> get props => [image];
}

class BeforeSimpanStok extends InputStokState {
  @override
  List<Object> get props => [];
}

class SimpanStokFailed extends InputStokState {
  String error;
  SimpanStokFailed({this.error});

  @override
  List<Object> get props => [error];
}

class SimpanStokSuccess extends InputStokState {
  String message;
  SimpanStokSuccess({this.message});

  @override
  List<Object> get props => [message];
}

class ChangeKategoriProduk extends InputStokState {
  String kategori;
  ChangeKategoriProduk({this.kategori});

  @override
  List<Object> get props => [kategori];
}

class ChangeTipe extends InputStokState {
  String tipe;
  ChangeTipe({this.tipe});

  @override
  List<Object> get props => [tipe];
}
