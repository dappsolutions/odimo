import 'package:equatable/equatable.dart';

abstract class InputStokEvent extends Equatable {
  const InputStokEvent();

  @override
  List<Object> get props => [];
}

class TakePictFromGallery extends InputStokEvent {}

class DecodeImageFromBase64 extends InputStokEvent {}

class SimpanInputStok extends InputStokEvent {}

class ChangeValueDefaultKategoriProduk extends InputStokEvent {}

class ChangeValueDefaultTipe extends InputStokEvent {}
