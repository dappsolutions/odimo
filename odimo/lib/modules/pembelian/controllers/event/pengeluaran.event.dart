import 'package:equatable/equatable.dart';

abstract class PengeluaranEvent extends Equatable {
  const PengeluaranEvent();

  @override
  List<Object> get props => [];
}

class SimpanPengeluaran extends PengeluaranEvent {}
