

import 'package:equatable/equatable.dart';

abstract class PembelianEvent extends Equatable{
  const PembelianEvent();

  @override
  List<Object> get props => [];
}

class ChangeValueTypeTransaction extends PembelianEvent{}

class SimpanPembelian extends PembelianEvent{}

