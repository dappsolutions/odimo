
import 'package:equatable/equatable.dart';


abstract class ListPembelianEvent extends Equatable{
  const ListPembelianEvent();

  @override
  List<Object> get props => [];
}

class GetDataProduk extends ListPembelianEvent{}