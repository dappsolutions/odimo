import 'package:equatable/equatable.dart';

abstract class ProdukStokEvent extends Equatable {
  const ProdukStokEvent();

  @override
  List<Object> get props => [];
}

class GetDataProduk extends ProdukStokEvent {}

class SimpanStokProduk extends ProdukStokEvent {}

class SimpanGambarStok extends ProdukStokEvent {
  var encodeImage;
  String codeDaster;

  SimpanGambarStok({this.encodeImage, this.codeDaster});
}
