import 'dart:io';

class InputStokModel {
  static String modules = "product_stok";
  static String nama_product = "";
  static String encodeImages = "";
  static String stok = "0";
  static String deskripsi = "";
  static File image;
  static var decodeImages;
  static String default_kategori = "TALI";
  static List<String> kategori_produk = ['TALI', 'YUKENSI', 'KANCING DEPAN'];
  static String default_tipe = "ADMIN";
  static List<String> tipe = ['ADMIN', 'RESELLER'];

  static Map<String, dynamic> params = {};
}
