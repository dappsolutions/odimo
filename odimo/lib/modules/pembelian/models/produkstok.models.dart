import 'package:template/storage/produkstok.dart';

class ProdukStokModel {
  static List<Produkstok> data = [];
  static bool isLoading = false;
  static String page = "1";
  static List<String> morelist = [
    'TALI',
    'YUKENSI',
    'KANCING DEPAN',
    'SEMUA',
    'CARI'
  ];
  static String filter_kategori = "";
  static String stok = "";
  static String product_stok_id = "";
  static Map<String, dynamic> params = {};
}
