class PembelianModel {
  static List<String> type_transactions = ['CASH', 'KREDIT', 'TRANSFER'];
  static List<String> morelist = [
    'Daftar Pembelian',
    'Daftar Produk Stok',
    'Input Stok',
    'Input Pengeluaran'
  ];

  static String default_trans = "CASH";

  static Map<String, dynamic> params = {};
  static String modules = "pembelian";
}
