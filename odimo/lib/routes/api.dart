import 'package:template/config/url.dart';
import 'package:template/modules/dashboard/models/dashboard.models.dart';
import 'package:template/modules/pembelian/models/inputstok.models.dart';
import 'package:template/modules/pembelian/models/pembelian.models.dart';
import 'package:template/modules/pembelian/models/pengeluaran.models.dart';

//untuk route url api per modules
class Api {
  static Map route = {
    DashboardModels.modules: {'getData': Url.appsurl + "/login/getData"},
    PembelianModel.modules: {
      'getData': (String page) {
        return Url.appsurl + "/pembelian/getData?page=${page}";
      },
      'getDataForAdmin': (String page) {
        return Url.appsurl + "/pembelian/getDataForAdmin?page=${page}";
      },
      'simpan': Url.appsurl + "/pembelian/simpan",
    },
    PengeluaranModel.modules: {
      // 'getData': (String page) {
      //   return Url.appsurl + "/pembelian/getData?page=${page}";
      // },
      'simpan': Url.appsurl + "/pengeluaran/simpan",
    },
    InputStokModel.modules: {
      'getData': (String page, String keyword) {
        return Url.appsurl +
            "/product_stok/getData?keyword=${keyword}&page=${page}";
      },
      'getDataForAdmin': (String page, String keyword) {
        return Url.appsurl +
            "/product_stok/getDataForAdmin?keyword=${keyword}&page=${page}";
      },
      'simpan': Url.appsurl + "/product_stok/simpan",
      'simpan_stok': Url.appsurl + "/product_stok/simpanStok",
    },
  };
}
